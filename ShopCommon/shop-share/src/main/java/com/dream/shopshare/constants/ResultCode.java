package com.dream.shopshare.constants;

public class ResultCode {
    /**
     * 成功
     */
    public static final int SUCCESS_CODE = 200;

    /**
     * 内部异常
     */
    public static final int INNER_ERROR = 1001;

    /**
     * 数据库异常
     */
    public static final int DB_ERROR = 1002;


}
