package com.dream.shopshare.exception;

import com.dream.shopshare.constants.ResultCode;

/**
 * 普通异常
 */
public class CException extends Exception {
    private int code = ResultCode.INNER_ERROR;

    private Exception e;

    private String message;

    public int getCode() {
        return code;
    }

    public CException(int code) {
        this.code = code;
    }

    public CException(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public CException(Exception e, int code) {
        this.e = e;
        this.code = code;
    }

    public CException(Exception e, String message) {
        this.e = e;
        this.message = message;
    }

    public CException(int code, Exception e, String message) {
        this.code = code;
        this.e = e;
        this.message = message;
    }

    public CException(String message) {
        this.message = message;
    }
}
