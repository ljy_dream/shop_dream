package com.dream.shopshare.response;

import com.dream.shopshare.constants.ResultCode;

public class BaseResponse {
    /**
     * 返回码 成功
     */
    private Integer retCode = ResultCode.SUCCESS_CODE;

    /**
     * 返回信息 默认成功
     */
    private String retMsg = "SUCCESS";

    public Integer getRetCode() {
        return retCode;
    }

    public void setRetCode(Integer retCode) {
        this.retCode = retCode;
    }

    public String getRetMsg() {
        return retMsg;
    }

    public void setRetMsg(String retMsg) {
        this.retMsg = retMsg;
    }

    @Override
    public String toString() {
        return "BaseResponse{" +
                "retCode=" + retCode +
                ", retMsg='" + retMsg + '\'' +
                '}';
    }
}
