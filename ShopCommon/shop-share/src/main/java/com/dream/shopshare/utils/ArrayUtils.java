package com.dream.shopshare.utils;

import java.util.List;

/**
 * 集合工具类
 */
public class ArrayUtils {

    /**
     * 空集合
     *
     * @param list 集合
     * @return 为空
     */
    public static boolean isEmpty(List<?> list) {
        return list == null || list.size() <= 0;
    }

    /**
     * 数组为空或是 null
     *
     * @param objects 数组
     * @return 结果
     */
    public static boolean arrayIsEmpty(Object[] objects) {
        return objects == null || objects.length == 0;
    }
}
