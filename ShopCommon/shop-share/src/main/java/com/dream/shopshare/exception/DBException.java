package com.dream.shopshare.exception;

import com.dream.shopshare.constants.ResultCode;

/**
 * 访问数据库异常
 */
public class DBException extends Exception {
    private int code = ResultCode.DB_ERROR;

    public int getCode() {
        return code;
    }

    public DBException(int code, String message) {
        super(message);
        this.code = code;
    }
}
