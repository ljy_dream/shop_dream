package com.dream.shopshare.utils;

/**
 * 字符串工具类
 */
public class StringUtils {
    /**
     * 判断是否为 空 或是 null
     *
     * @param str 字符串
     */
    public static boolean isBlank(String str) {
        return str == null || str.trim().length() == 0;
    }

    /**
     * 字节组转化成16进制字符串
     *
     * @param byteArray 字节组
     * @return 16进制字符串
     */
    public static String toHexString(byte[] byteArray) {
        if (byteArray == null || byteArray.length == 0) {
            return null;
        }

        StringBuffer sb = new StringBuffer(byteArray.length);
        for (byte b : byteArray) {
            sb.append(String.format("%02X", b));
        }
        return sb.toString();
    }

}
