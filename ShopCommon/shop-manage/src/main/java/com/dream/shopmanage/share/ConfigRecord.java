package com.dream.shopmanage.share;

/**
 * t_config配置项
 */
public class ConfigRecord {
    /**
     * 主键
     */
    private Long id;

    /**
     * key
     */
    private String configKey;

    /**
     * 值
     */
    private String configValue;

    /**
     * 默认值
     */
    private String configDefaultValue;

    /**
     * 注释
     */
    private String des;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public String getConfigValue() {
        return configValue;
    }

    public void setConfigValue(String configValue) {
        this.configValue = configValue;
    }

    public String getConfigDefaultValue() {
        return configDefaultValue;
    }

    public void setConfigDefaultValue(String configDefaultValue) {
        this.configDefaultValue = configDefaultValue;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    @Override
    public String toString() {
        return "ConfigRecord{" +
                "id=" + id +
                ", configKey='" + configKey + '\'' +
                ", configValue='" + configValue + '\'' +
                ", configDefaultValue='" + configDefaultValue + '\'' +
                ", desc='" + des + '\'' +
                '}';
    }
}
