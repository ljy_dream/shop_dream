package com.dream.member.service.api.member;

import com.dream.member.service.api.member.request.AddMemberReq;
import com.dream.member.share.member.MemberRecord;
import com.dream.shopshare.response.BaseResponse;

import java.util.List;

/**
 * 会员服务
 */
public interface MemberService {
    /**
     * 查询会员列表
     * @return list
     */
    List<MemberRecord> queryMembers();

    /**
     * 添加会员
     * @param req req
     * @return rsp
     */
    BaseResponse addMember(AddMemberReq req);
}
