package com.dream.member.service.impl.member;

import com.dream.member.dao.member.MemberDao;
import com.dream.member.service.api.member.MemberService;
import com.dream.member.service.api.member.request.AddMemberReq;
import com.dream.member.share.Constant;
import com.dream.member.share.member.MemberRecord;
import com.dream.member.util.ConfigUtil;
import com.dream.shopshare.SnowFlakeUtil;
import com.dream.shopshare.exception.CException;
import com.dream.shopshare.exception.DBException;
import com.dream.shopshare.response.BaseResponse;
import com.dream.shopshare.utils.EncryptUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("memberService")
public class MemberServiceImpl implements MemberService {
    @Autowired
    private MemberDao memberDao;

    public List<MemberRecord> queryMembers() {
        return memberDao.queryMembers(new MemberRecord());
    }

    public BaseResponse addMember(AddMemberReq req) {
        BaseResponse response = new BaseResponse();
        MemberRecord record;
        try {
            record = buildMemberRecord(req);
            memberDao.addMember(record);
        } catch (CException e) {
            response.setRetCode(e.getCode());
            response.setRetMsg(e.getMessage());
        } catch (DBException e) {
            response.setRetCode(e.getCode());
            response.setRetMsg("db Exception");
        }

        return response;
    }

    private MemberRecord buildMemberRecord(AddMemberReq req) throws CException {
        MemberRecord record = req.getMemberRecord();

        record.setId(SnowFlakeUtil.generateId());
        record.setPassword(EncryptUtil.getInstance().MD5(record.getPassword(), ConfigUtil.getConfigValue(Constant.MD5_KEY)));
        record.setStatus(Constant.STATUS_OPEN);

        return record;
    }


}
