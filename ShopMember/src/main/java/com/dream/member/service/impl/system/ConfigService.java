package com.dream.member.service.impl.system;

import com.dream.member.dao.system.ConfigDao;
import com.dream.shopmanage.share.ConfigRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("configService")
public class ConfigService {
    @Autowired
    private ConfigDao configDao;

    public List<ConfigRecord> queryConfigs() {
        return configDao.queryConfigs();
    }
}
