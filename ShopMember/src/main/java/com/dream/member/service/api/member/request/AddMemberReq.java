package com.dream.member.service.api.member.request;

import com.dream.member.share.member.MemberRecord;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 添加会员请求
 */
public class AddMemberReq {
    @NotNull
    @Valid
    private MemberRecord memberRecord;

    public MemberRecord getMemberRecord() {
        return memberRecord;
    }

    public void setMemberRecord(MemberRecord memberRecord) {
        this.memberRecord = memberRecord;
    }

    @Override
    public String toString() {
        return "AddMemberReq{" +
                "memberRecord=" + memberRecord +
                '}';
    }
}
