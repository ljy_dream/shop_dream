package com.dream.member.controller.member;

import com.dream.member.service.api.member.MemberService;
import com.dream.member.service.api.member.request.AddMemberReq;
import com.dream.member.share.member.MemberRecord;
import com.dream.member.util.ConfigUtil;
import com.dream.shopshare.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/shopMember")
public class MemberController {
    @Autowired
    private MemberService memberService;

    @PostMapping("/query")
    public List<MemberRecord> query() {
        return memberService.queryMembers();
    }

    @PostMapping("/addShopMember")
    public BaseResponse addShopMember(@RequestBody @Valid AddMemberReq req) {
        return memberService.addMember(req);
    }

    @PostMapping("/config")
    public String getConfigs() {
        return ConfigUtil.getConfigValue("test");
    }

}
