package com.dream.member;

import com.dream.member.util.SpringContextUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
@EnableEurekaClient
public class ShopMemberApp {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(ShopMemberApp.class, args);
        SpringContextUtil.setContext(context);
    }
}
