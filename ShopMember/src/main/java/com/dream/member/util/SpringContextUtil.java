package com.dream.member.util;

import org.springframework.context.ApplicationContext;

/**
 * spring上下文工具
 */
public class SpringContextUtil {
    private static ApplicationContext context;

    public static ApplicationContext getContext() {
        return context;
    }

    public static <T> T getBean(String beadId, Class<T> clazz) {
        // 用于记录休眠次数，只允许重试100次
        int count = 100;
        while (null == SpringContextUtil.context && count > 0) {
            try {
                System.out.println("context is null");
                count -- ;
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return context.getBean(beadId, clazz);
    }

    public static void setContext(ApplicationContext context) {
        if (SpringContextUtil.context == null && context != null) {
            SpringContextUtil.context = context;
        }
    }
}
