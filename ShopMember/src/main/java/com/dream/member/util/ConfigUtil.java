package com.dream.member.util;

import com.dream.member.service.impl.system.ConfigService;
import com.dream.shopmanage.share.ConfigRecord;
import com.dream.shopshare.utils.ArrayUtils;
import com.dream.shopshare.utils.StringUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 获取配置项的工具
 */
@Configuration
public class ConfigUtil {
    private static Map<String, String> map = new HashMap<String, String>();

    public ConfigUtil() {
    }

    /**
     * 定时，5分钟刷新一次
     */
    @Scheduled(fixedRate = 5 * 60 * 1000)
    private void updateConfig() {
        ConfigService configService = SpringContextUtil.getBean("configService", ConfigService.class);
        List<ConfigRecord> records = configService.queryConfigs();
        if (!ArrayUtils.isEmpty(records)) {
            for (ConfigRecord record : records) {
                if (!StringUtils.isBlank(record.getConfigValue())) {
                    map.put(record.getConfigKey(), record.getConfigValue());
                } else {
                    map.put(record.getConfigKey(), record.getConfigDefaultValue());
                }
            }
        }
    }

    /**
     * 获取配置项
     *
     * @param key 键
     * @return 值
     */
    public static String getConfigValue(String key) {
        return map.get(key);
    }
}
