package com.dream.member.share;

/**
 * 会员常量
 */
public class Constant {
    /**
     * md5的加密秘药
     */
    public static String MD5_KEY = "md5_key";

    /**
     * 状态；开启
     */
    public static int STATUS_OPEN = 1;
}
