package com.dream.member.dao.member;

import com.dream.member.share.member.MemberRecord;
import com.dream.shopshare.exception.DBException;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MemberDao {
    /**
     * 添加用户
     *
     * @param record 用户记录
     * @throws Exception 数据库异常
     */
    void addMember(MemberRecord record) throws DBException;

    List<MemberRecord> queryMembers(MemberRecord record);


}
