package com.dream.member.dao.system;

import com.dream.shopmanage.share.ConfigRecord;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ConfigDao {
    List<ConfigRecord> queryConfigs();

}
